<?php

namespace Inter;

interface controller {
    public function view_main();
    public function handles_own_nav();
}
