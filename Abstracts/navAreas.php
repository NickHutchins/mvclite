<?php

interface navAreas {
    function addsLinks();
    function removeLinks();
    function allowedNavs();
    function disabledNavs(); 
}