<?php

    class GenericException extends Exception {

        protected $iID;
        protected $sMessage;
        protected $toggle;

        public function __construct($sCode = "", $sMessage = "", $component = ""){

            /*build the exception message */
            $this->sMessage .= "<h1>Exception Thrown</h1><p><strong>".$sCode."</strong>".$sMessage."</p>";
            $aVarsAtThrow['DV'] = get_defined_vars();
            $aVarsAtThrow['P'] = $_POST;
            $aVarsAtThrow['G'] = $_GET;
            $aVarsAtThrow['S'] = $_SERVER;
            $aVarsAtThrow['SS'] = $_SESSION ?? array();
            $aVarsAtThrow['C'] = $_COOKIE;
            $aVarsAtThrow['F'] = $_FILES;
            $aConstants = get_defined_constants(true);
            $aVarsAtThrow['CO'] = $aConstants['user'];
            $aVarsAtThrow['HE'] = getallheaders();
            try{
                $aVarsAtThrow['B'] = @get_browser(null, true);
            }catch(Exception $e){
                $aVarsAtThrow['B'] = "No Browser Info Supplied.";
            }

            unset($aVarsAtThrow['HE']['Cookie']);

            /* Processing Stack trace */
            $aStackTrace = debug_backtrace();
            array_shift($aStackTrace);

            foreach ($aStackTrace as $iLevel => $aLevelInfo) {
                if(isset($aLevelInfo['line'])){
                    $line = $aLevelInfo['line'];
                }else{
                    $line = "";
                }

                if(isset($aLevelInfo['class'])){
                    $class = $aLevelInfo['class'];
                }else{
                    $class = "";
                }
                $this->sMessage .= "#" . $iLevel . ": On Line " . $line . " Caller->  " . $class . "->" . $aLevelInfo['function'] . "() </br>";
            }

            /* Processing Environment Variable */
            $this->sMessage .= "<h2>Environment Variables</h2>";

            $this->addArrayInfo($aVarsAtThrow['P'], "Get Variables");
            $this->addArrayInfo($aVarsAtThrow['G'], "Post Variables");
            $this->addArrayInfo($aVarsAtThrow['S'], "Server Variables");
            $this->addArrayInfo($aVarsAtThrow['SS'], "Session Variables");
            $this->addArrayInfo($aVarsAtThrow['C'], "Cookies");
            $this->addArrayInfo($aVarsAtThrow['F'], "Posted Files");
            $this->addArrayInfo($aVarsAtThrow['DV'], "Other Defined Variables");
            $this->addArrayInfo($aVarsAtThrow['CO'], "Constants");
            $this->addArrayInfo($aVarsAtThrow['HE'], "Input headers");
            $this->addArrayInfo($aVarsAtThrow['B'], "Browser Information");

            /*
             * Would thrown an excception if the insert failed, but yeah.
             * Add Exception into DB.
             */

            $data['code'] = $sCode;
            $data['message'] = $this->sMessage;

            if($component != "") {
                $data['component'] = $component;
            }

            $iInsertID = CRUD::create(TABLEPREFIX."_exceptions", $data);
            $this->iID = rand()."-".$iInsertID."-".rand();

        }

        public function getID(){
            return $this->iID;
        }

        private function getFrontFacingErrorPageContent(){
            /*
             * User Based Exception Message:
             * Clear all page content to prevent hackery (If there ever was any..)
             * New Blurred version of site as backround.
             * Centralised Div with error message and link back to home.
             */
            $aHeaders = getallheaders();
            $sBrowser = $aHeaders['User-Agent'];


            $return = '<script>$("html").empty();</script>'
                . '<div style="width:100%; height:100%; position:fixed; left:0; top:0; z-index:-1;"><img src="" style="width:100%;"/></div>'
                . '<div style="width:900px; margin: 0 auto; margin-top:200px; padding:30px; background-color:#fff; border: 10px solid rgba(200, 200, 200, .5); -webkit-background-clip: padding-box;  background-clip: padding-box; /* for IE9+, Firefox 4+, Opera, Chrome */ box-shadow:5px 5px 5px;">'
                . '<h1>Sorry!</h1>'
                . "<h3>We we're unable to complete this request successfully.</h3>"
                . '<p>'
                . 'If you find that this problem persists, you may like to <a href="#">contact us</a> for support.<br/>'
                . 'When contacting us for support, please provide as much of the following information as possible -- in particular the <strong>error code</strong><br/><br/><br/>'
                . '<strong> Error Code </strong>'.$this->iID.'<br/><br/>'
                . '<strong> Date </strong>'.date('d-m-Y').'<br/><br/>'
                . '<strong> Time </strong>'.date('H:i:s').'<br/><br/>'
                . '<strong> URI </strong>'.$_SERVER['REQUEST_URI'].'<br/><br/>'
                . '<strong> Your Browser </strong>'.$sBrowser
                . '</p>'
                . "</div></br><br/><br/><br/>";

            if($this->toggle){
                $return .=$this->sMessage;
            }
            return $return;
        }

        private function addArrayInfo($array, $name) {
            $this->sMessage .= "<h3>" . $name . "</h3>";
            if ($array) {
                foreach ($array as $sArrayKey => $sArrayItem) {
                    $this->sMessage .= "&nbsp;&nbsp;&nbsp;<strong>" . ucfirst($sArrayKey) . "</strong>: ";
                    if (is_string($sArrayItem)) {
                        $this->sMessage .= $sArrayItem . "<br/>";
                    } else {
                        $this->sMessage .= "<pre>".var_export($sArrayItem, true)."</pre>";
                    }
                }
            } else {
                $this->sMessage .= "No " . $name;
            }
        }

        public function getExceptionMessage(){
                return $this->getFrontFacingErrorPageContent();
        }

        public static function getExceptionInfo($ID){
            $WH['ID'] = $ID;
            $data =  CRUD::retrieve(TABLEPREFIX.'_exceptions', $WH, true);
            return $data;
        }


    }
