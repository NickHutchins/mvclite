<?php

class ErrorHandler{

    public static function HandlePHPError($errno, $errstr, $errfile, $errline ) {
        if($errno == 0 || $errno == 2 || $errno == 8){
            return;
        }

        if(!StringFunctions::endsWith($errfile, "Core\requires.php")) {
            //DOESNT NEED TO BE IN TRY CATCH AS IT REMAINS IN THE CODE CONTEXT
            $className = EXCEPTIONCLASS;
            throw new $className("Error - " . $errno, "<strong>File:</strong> " . $errfile . " <strong>Line: </strong>" . $errline . " - " . $errstr, "Core Error");
        }
    }

    public static function HandlePHPFatal(){
        $error = error_get_last();
        $className = EXCEPTIONCLASS;
        if ($error['type'] == 1) {
            $errfile = $error["file"];
            $errline = $error["line"];
            $errstr  = $error["message"];

            $exception = new $className(" Fatal Error - 1", "<strong>File:</strong> ".$errfile." <strong>Line: </strong>".$errline." - ".$errstr, "Core Error");
            $expl = explode("/",$_SERVER['PHP_SELF']);

            switch(array_pop($expl)){
                case "index.php":
                    echo $e->getFrontFacingErrorContent();
                    break;
            }
        }
    }
}


