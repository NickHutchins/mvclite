<?php

    /* CREATE RETRIEVE UPDATE DELETE */

    class CRUD {

        static $connection;

        private static function init()
        {
            //if(!CRUD::$connection){
            //    CRUD::$connection = mysqli_connect(DBHOST, DBUSERNAME, DBPASSWORD, DBNAME);
            //}
            if(!CRUD::$connection) {
                CRUD::$connection = new \PDO("mysql:host=".DBHOST.";dbname=".DBNAME.";charset=utf8", DBUSERNAME, DBPASSWORD);
                CRUD::$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            }

        }

        public static function manualQuery($S_ManQuery, $return = true, $oneRow = false)
        {
            //echo $S_ManQuery;
            CRUD::init();
            $stmt = CRUD::$connection->prepare($S_ManQuery);
            $stmt->execute();

            if($return) {
                while(($row = $stmt->fetch(\PDO::FETCH_ASSOC)) != false) {
                    $rows[] = $row;
                }
                if($oneRow) {
                    if(sizeof($rows) == 1) {
                        $rows = $rows[0];
                    }
                }

                return $rows;
            } else {
                return true;
            }
        }

        public static function update($table, $update, $find)
        {
            CRUD::init();
            if($update && is_array($update)) {
                foreach($update as $updateK => $updateV) {
                    if(strtolower($updateV) == "null") {
                        $updateSTR[] = " `".$updateK."` = NULL ";
                    } else {
                        $executeArray[':'.$updateK."U"] = $updateV;
                        $updateSTR[]                    = " `".$updateK."` = :".$updateK."U ";
                    }
                }
            } else {
                throw new StockCheckerException("CRUD:Update called Incorrectly", "Variables to be Updated were not Valid (either not present or not an array)");
            }
            if($find && is_array($find)) {
                foreach($find as $findK => $findV) {
                    if($findK == 1 && $findV == 1) {
                        unset($whereSTR);
                        $whereSTR = " 1=1";
                    }else{
                        if(strtolower($findV) == "null") {
                            $whereSTR[] = " `".$findK."` IS NULL ";
                        } else {
                            $findArray[":".$findK."W"] = $findV;
                            $whereSTR[]                = " `".$findK."` = :".$findK."W ";
                        }
                    }
                }
            } else {
                throw new StockCheckerException("CRUD:Update called Incorrectly", "WHERE variables were incorrectly defined or not defined at all (TO force please use 1=1)");
            }

            $finalQ = "UPDATE ".$table." SET ".implode(",", $updateSTR)." WHERE ".implode("AND", $whereSTR);

            $PDOEX = array_merge($executeArray, $findArray);
            $PreppedStatement = CRUD::$connection->prepare($finalQ);
            return $PreppedStatement->execute($PDOEX);
        }

        public static function delete($table, $find)
        {
            if($find && is_array($find)) {
                foreach($find as $findK => $findV) {
                    if($findK == '1' && $findV == '1') {
                        unset($whereSTR);
                        $whereSTR[] = " 1 = 1";
                    }else{
                        if(strtolower($findV) == "null") {
                            $whereSTR[] = " `".$findK."` IS NULL ";
                        } else {
                            $findArray[":".$findK."W"] = $findV;
                            $whereSTR[]                = " `".$findK."` = :".$findK."W ";
                        }
                    }

                }
            } else {
                throw new StockCheckerException("CRUD:Delete called Incorrectly", "WHERE variables were incorrectly defined or not defined at all (TO force please use 1=1)");
            }

            $finalQ = "DELETE FROM ".$table." WHERE".implode("AND", $whereSTR);
            //echo $finalQ;
            $PDOEX = $findArray;

            $PreppedStatement = CRUD::$connection->prepare($finalQ);
            $PreppedStatement->execute($PDOEX);
        }

        public static function insertIgnore($table,$values)
        {
            CRUD::init();
            if(!is_array($values)) {
                throw new StockCheckerException("CRUD:Create called Incorrectly", "Insert Command was not an array.");
            }

            $columns = "";
            $string  = "";
            foreach($values as $key => $val) {

                $columns .= " `".$key."`,";

                $placeholderName = ":Nval".$key;

                $string .= " ".$placeholderName.",";

                $insertArray[$placeholderName] = $val;
            }

            $string  = substr($string, 0, -1);
            $columns = substr($columns, 0, -1);


            $finalQ = 'INSERT IGNORE INTO `'.$table.'` ('.$columns.') VALUES ('.$string.')';
            $PreppedStatement = CRUD::$connection->prepare($finalQ);
            $PreppedStatement->execute($insertArray);

            return CRUD::$connection->lastInsertID();
        }

        public static function create($table, $values)
        {
            CRUD::init();
            if(!is_array($values)) {
                throw new StockCheckerException("CRUD:Create called Incorrectly", "Insert Command was not an array.");
            }

            $columns = "";
            $string  = "";
            foreach($values as $key => $val) {

                $columns .= " `".$key."`,";

                $placeholderName = ":Nval".$key;

                $string .= " ".$placeholderName.",";

                $insertArray[$placeholderName] = $val;
            }

            $string  = substr($string, 0, -1);
            $columns = substr($columns, 0, -1);


            $finalQ = 'INSERT INTO `'.$table.'` ('.$columns.') VALUES ('.$string.')';
            $PreppedStatement = CRUD::$connection->prepare($finalQ);
            $PreppedStatement->execute($insertArray);

            return CRUD::$connection->lastInsertID();
        }

        public static function retrieve($table, $find = NULL, $oneRow = false, $sortBy = NULL, $ASC = "ASC", $limit = null, $abs = false)
        {
            CRUD::init();

            $findArray = null;

            if($find && is_array($find)) {
                foreach($find as $findK => $findV) {
                    if($findK == '1' && $findV == '1') {
                        unset($whereSTR);
                        $whereSTR[] = " 1 = 1";
                        break;
                    }else{
                        if(is_array($findV)){
                            $value = $findV[0];
                            $op = $findV[1];

                            if($value == "NOW()"){
                                $whereSTR[] = " `".$findK."` ".$op." NOW() ";
                            }else{
                                $findArray[":".$findK."W"] = $value;
                                $whereSTR[] = " `".$findK."` ".$op." :".$findK."W ";
                            }


                        }else{
                            if(strtolower($findV) == "null") {
                                $whereSTR[] = " `".$findK."` IS NULL ";
                            } else {
                                $findArray[":".$findK."W"] = $findV;
                                $whereSTR[] = " `".$findK."` = :".$findK."W ";
                            }
                        }
                    }
                }
            } else {
                $whereSTR[] = " 1 = 1";
            }

            if($sortBy == "RAND()") {
                $ob = " ORDER BY RAND() ";
            }else if($sortBy) {
                if($abs){
                    $ob = " ORDER BY ABS(`".$sortBy."`) ".$ASC;
                }else{
                    $ob = " ORDER BY `".$sortBy."` ".$ASC;
                }

            } else {
                $ob = "";
            }

            if(is_numeric($limit)){
                $li = " LIMIT ".$limit." ";
            }else{
                $li = "";
            }

            $finalQ = "SELECT * FROM ".$table." WHERE".implode(" AND ", $whereSTR).$ob.$li.";";

            $PDOEX = $findArray;

            //echo $finalQ;

            $PreppedStatement = CRUD::$connection->prepare($finalQ);
            $PreppedStatement->execute($PDOEX);

            $rows = array();

            while(($row = $PreppedStatement->fetch(\PDO::FETCH_ASSOC)) != false) {
                $rows[] = $row;
            }

            if($oneRow) {
                if(sizeof($rows) == 1) {
                    $rows = $rows[0];
                }
            }

            return $rows;

        }

        public static function search($table, $ss, $find = NULL, $ignoredCols = NULL)
        {
            CRUD::init();
            if($ignoredCols) {
                foreach($ignoredCols as &$column) {
                    $column = strtolower($column);
                }
            }

            $q   = "SHOW columns FROM `".$table."`";
            $res = CRUD::manualQuery($q);

            foreach($res as $key => $col) {
                if($ignoredCols) {
                    $lcCols = strtolower($col['Field']);
                    if(in_array($lcCols, $ignoredCols)) {
                        unset($res[$key]);
                    }
                }
            }



            if($find && is_array($find)) {
                foreach($find as $findK => $findV) {
                    if($findK == 1 && $findV == 1) {
                        unset($whereSTR);
                        $whereSTR = "1=1";
                    }else{
                        if(strtolower($findV) == "null") {
                            $whereSTR[] = " `".$findK."` IS NULL ";
                        } else {
                            $findArray[":".$findK."W"] = $findV;
                            $whereSTR[]                = " `".$findK."` = :".$findK."W ";
                        }
                    }
                }

                $where = " AND ".implode(" AND ",$whereSTR);
            }else{
                $where = "";
            }


            foreach($res as $key => $col) {
                $searchString[] = " (`".$col['Field']."` LIKE :searchString) ";
            }

            $implSS      = implode(" OR ", $searchString);
            $ssSearch[':searchString'] = "%".$ss."%";


            if($find){
                $PDOEX = array_merge($findArray, $ssSearch);
            }else{
                $PDOEX = $ssSearch;
            }


            $finalQ = "SELECT * FROM `".$table."` WHERE (".$implSS.")".$where.";";
            $PreppedStatement = CRUD::$connection->prepare($finalQ);
            $PreppedStatement->execute($PDOEX);

            $rows = array();

            while(($row = $PreppedStatement->fetch(\PDO::FETCH_ASSOC)) != false) {
                $rows[] = $row;
            }


            return $rows;

        }

        public static function max($table, $col, $find = NULL)
        {
            CRUD::init();

            $finalQ = "SELECT MAX(".$col.") FROM ".$table;
            // echo $finalQ;


            $PreppedStatement = CRUD::$connection->prepare($finalQ);
            $PreppedStatement->execute();

            $row = $PreppedStatement->fetch(\PDO::FETCH_ASSOC);

            if($row){
                return $row['MAX('.$col.')'];
            }else{
                return 0;
            }

        }

        public static function sum($table, $columnToCount, $find = NULL,  $sortBy = NULL, $ASC = "ASC", $limit = null){

                CRUD::init();

                $findArray = null;

                if($find && is_array($find)) {
                    foreach($find as $findK => $findV) {
                        if($findK == '1' && $findV == '1') {
                            unset($whereSTR);
                            $whereSTR[] = " 1 = 1";
                        }else{
                            echo '';
                            if(strtolower($findV) == "null") {
                                $whereSTR[] = " `".$findK."` IS NULL ";
                            } else {
                                $findArray[":".$findK."W"] = $findV;
                                $whereSTR[] = " `".$findK."` = :".$findK."W ";
                            }
                        }
                    }
                } else {
                    throw new StockCheckerException("CRUD:Retrieve called Incorrectly", "WHERE variables were incorrectly defined or not defined at all (TO force please use 1=1)");
                }

                if($sortBy == "RAND()") {
                    $ob = " ORDER BY RAND() ";
                }else if($sortBy) {
                    $ob = " ORDER BY `".$sortBy."` ".$ASC;
                } else {
                    $ob = "";
                }

                if(is_numeric($limit)){
                    $li = " LIMIT ".$limit." ";
                }else{
                    $li = "";
                }

                $finalQ = "SELECT SUM(`".$columnToCount."`) FROM(SELECT * FROM ".$table." WHERE".implode(" AND ", $whereSTR).$ob.$li.") as count;";

                $PDOEX = $findArray;

                //echo $finalQ;

                $PreppedStatement = CRUD::$connection->prepare($finalQ);
                $PreppedStatement->execute($PDOEX);

                $rows = array();

                $row = $PreppedStatement->fetch(\PDO::FETCH_ASSOC);

                return $row['SUM(`'.$columnToCount.'`)'];

        }

        public static function numRows($table, $find = null)
        {
            CRUD::init();

            $findArray = null;

            if($find && is_array($find)) {
                foreach($find as $findK => $findV) {
                    if(strtolower($findV) == "null") {
                        $whereSTR[] = " `".$findK."` IS NULL ";
                    } else {
                        $findArray[":".$findK."W"] = $findV;
                        $whereSTR[] = " `".$findK."` = :".$findK."W ";
                    }
                }
            } else {
                unset($whereSTR);
                $whereSTR[] = " 1 = 1";
            }


            $finalQ = "SELECT * FROM ".$table." WHERE".implode(" AND ", $whereSTR).";";

            $PDOEX = $findArray;

            //echo $finalQ;

            $PreppedStatement = CRUD::$connection->prepare($finalQ);
            $PreppedStatement->execute($PDOEX);

            $rows = array();

            while(($row = $PreppedStatement->fetch(\PDO::FETCH_ASSOC)) != false) {
                $rows[] = $row;
            }

            if($rows) {
                return sizeof($rows);
            }else{
                return 0;
            }
        }

        public static function getIDs($table)
        {
            CRUD::init();
            $result = CRUD::manualQuery("SELECT * FROM ".$table);
            if($result) {
                foreach($result as $row){
                    $IDS[sizeof($IDS)] = $row['ID'];
                }
            }else{
                throw new StockCheckerException("getIDS CRUD","Invalid Database Response Recieved");
            }

            return $IDS;
        }

        public static function tableExists($table){

            $finalQ = "SELECT * FROM `information_schema`.`TABLES` WHERE `TABLE_NAME` = :FINT && `TABLE_SCHEMA` = '".DBNAME."'";
            $findArray[':FINT'] = $table;
            $PDOEX = $findArray;

            //echo $finalQ;

            $PreppedStatement = CRUD::$connection->prepare($finalQ);
            $PreppedStatement->execute($PDOEX);


            $rows = array();

            while(($row = $PreppedStatement->fetch(\PDO::FETCH_ASSOC)) != false) {
                return true;
            }


            return false;
        }

    }
