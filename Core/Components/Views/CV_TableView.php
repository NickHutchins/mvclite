<?php
$buttons = array(
    'Download Report' => array('icon' => 'download', 'type' => 'blue'),
    'Results' => array('icon' => 'flag', 'type' => 'success'),
    'Suspend' => array('icon' => 'minus-circled', 'type' => 'danger'),
    'End' => array('icon' => 'cancel-circled', 'type' => 'black'),
    'Remind User to Take Part' => array('icon' => 'bell', 'type' => 'orange'),
    'Delete' => array('icon' => 'cancel-circled', 'type' => 'danger'),
    'Remove Account' => array('icon' => 'cancel-circled', 'type' => 'danger'),
    'Modify' => array('icon' => 'pencil', 'type' => 'info'),
    'Edit' => array('icon' => 'pencil', 'type' => 'info'),
    'View Details' => array('icon' => 'eye', 'type' => 'info'),
    'View' => array('icon' => 'eye', 'type' => 'info'),
    'View/Modify' => array('icon' => 'eye', 'type' => 'info'),
    'Fixed' => array('icon' => 'check', 'type' => 'success'),
    'Use' => array('icon' => 'thumbs-up', 'type' => 'success'),
    'Queue' => array('icon' => 'dot-3', 'type' => 'primary'),
    'De-Queue' => array('icon' => 'dot', 'type' => 'primary'),
    'Renew' => array('icon' => 'thumbs-up', 'type' => 'success'),
    'Pause' => array('icon' => 'pause', 'type' => 'primary'),
    'Unsuspend' => array('icon' => 'thumbs-up', 'type' => 'success'),
    '(Re)Send Activation Link' => array('icon' => 'link', 'type' => 'info')
);

if (sizeof($tableData) == 0) { ?>
    <center><strong>There are no <?= $tableName ?>. </strong></center>
<?php } else { ?>

    <?php $uid = sha1($tableName.$_GET['controller'].$_GET['view']);

    $cvar = 'processArgsIntoString';
    if (!is_callable($cvar)) {
        function processArgsIntoString($argument, $tableRow, $includeSer = false)
        {
            if (stristr($argument, "|")) {
                $subArgs = array();
                $sa = explode("|", $argument);
                foreach ($sa as $subArg) {
                    if (stristr($subArg, "arg:")) {
                        $subArgs[] = $tableRow[substr($subArg, 4)];
                    } else {
                        $subArgs[] = $subArg;
                    }
                }
                $args = "'" . implode("|", $subArgs) . "'";

            } else if (stristr($argument, "&") && $includeSer) {

                $explode = explode("&", $argument);
                foreach ($explode as $subArg2) {
                    if (stristr($subArg2, "=")) {
                        $explode2 = explode("=", $subArg2);
                        $key = $explode2[0];
                        $val = $explode2[1];
                        if (stristr($val, "arg:")) {
                            $val = $tableRow[substr($val, 4)];
                        }
                        $fs[] = $key . "=" . $val;
                    } else {
                        $fs[] = $subArg2;
                    }
                }


                $args = "'" . implode("&", $fs) . "'";

            } else if(stristr($argument, "=") && $includeSer) {

                $explode2 = explode("=", $argument);
                $key = $explode2[0];
                $val = $explode2[1];
                if (stristr($val, "arg:")) {
                    $val = $tableRow[substr($val, 4)];
                }
                $args = "'" . $key . "=" . $val . "'";

            } else if(stristr($argument, "(arg:")){

                $argsSplit = explode("(arg:",$argument);
                $latter = explode(")",$argsSplit[1]);

                $requested = $latter[0];
                $args = "'" . $argsSplit[0].$tableRow[$requested].$latter[1]."'";

            }else{
                if (stristr($argument, "arg:")) {
                    $args = "'" . $tableRow[substr($argument, 4)] . "'";
                } else {
                    $args = "'" . $argument . "'";
                }
            }

            return $args;
        }
    }

    ?>

    <script>
        $(function () {
            var panelList = $('.draggablePanelList');
            panelList.sortable({
                handle: '.table-draggableTab',
                update: function () {
                    $('#newOrderSaveButton').removeAttr('disabled');        //TODO: BUTTON THAT CALLS saveNewOrder WITH IS ID NEEDS TO BE BUILT INTO THE TABLE OBJECT...
                }
            });

            $.fn.dataTable.moment('DD-MM-YYYY HH:mm:ss');
            $.fn.dataTable.moment('DD-MM-YYYY');

            var dataTableX = $("#table<?=$uid?>").DataTable({
                    <?php if(isset($tableOptions['draggable']['save']['command'])){?>
                    "paging": false,
                    "ordering": false,
                    <?php } ?>
                    <?php if($tableOptions['compact']){ ?>
                    "ordering": false,
                    "paging": false,
                    "info": false,
                    "bFilter":false,
                    <?php } ?>
                    <?php if($tableOptions['noordering']){ ?>
                    "ordering": false,
                    <?php } ?>
                    <?php if($tableOptions['nopaging']){ ?>
                    "paging": false,
                    <?php } ?>
                    <?php if($tableOptions['noinfo']){ ?>
                    "info": false,
                    <?php } ?>
                    <?php if($tableOptions['nofilter']){ ?>
                    "bFilter":false,
                    <?php } ?>
                    "aaSorting": [],
                    "pagingType": "full_numbers",
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "stateSave": true
                }
            );

            dataTableX.row.add( [ 'Fiona White', 32, 'Edinburgh','ss','ss' ,"44","25"]);
            dataTableArray.push(dataTableX);

            <?php if(!$tableOptions['compact']){ ?>

                $("#table<?=$uid?>").dataTable().columnFilter({
                    "sPlaceHolder": "head:after"
                });
            <?php } ?>

        });

        <?php if(isset($tableOptions['draggable']['save']['command'])){?>

        function saveNewOrder() {

            var i = 1;
            var arr = [];

            $('#wholeTableContainer<?=$uid?> .table-dataRow').each(function (index, elem) {
                var itemID = $(elem).attr('ID');
                var orderID = i;
                arr.push(itemID);
                i++;
            });

            //doAct('<?=$tableOptions['draggable']['save']['command']?>', 'arr=' + arr);
        }

        //doAct('<?=$tableOptions['draggable']['save']['command']?>', 'subItemID='+itemID+'&newOrder='+orderID);

        <?php } ?>

        var ROW<?=$uid?> = [];

        function toggleTicks<?=$uid?>(tickbox) {
            var tickedID = $(tickbox).attr("data-attr");
            if ($(tickbox).is(":checked")) {
                var find = ROW<?=$uid?>.indexOf(tickedID);
                if (find == -1) {
                    ROW<?=$uid?>.push(tickedID);
                }
            } else {
                var find = ROW<?=$uid?>.indexOf(tickedID);
                if (find != -1) {
                    ROW<?=$uid?>.splice(find, 1);
                }
            }
        }

        function showHide<?=$uid?>() {
            $("#wholeTableContainer<?=$uid?>").slideToggle(300);
        }
    </script>



    <div class="panel minimal minimal-gray" data-collapsed="0">
        <div class="panel-heading">
            <div class="panel-options">
                <?php if (sizeof($tableData) != 0 && isset($tableOptions['bulk'])) { ?>
                    <?php if ($tableBulks) { ?>
                        <?php foreach ($tableBulks as $tb) {

                            $args = array();
                            foreach ($tb['action']['args'] as $argument) {
                                if ($argument == "{list}") {
                                    $argument = "'selectedList='+(ROW" . $uid . ".join())";
                                }

                                $args[] = $argument;
                            }

                            $args = implode(", ", $args);
                            ?>

                            <div class="btn btn-primary" onclick="<?= $tb['action']['function'] ?>(<?= $args ?>);"><?= $tb['text'] ?></div>


                        <?php } ?>
                    <?php } ?>
                <?php } ?>

            </div>
         </div>

        <!-- panel body -->
        <div class="panel-body">


            <div class="table-resp" id="wholeTableContainer<?= $uid ?>">
                <table class="table table-bordered datatable responsive tablesorter" id="DATAtable<?=str_replace(" ","-",$tableName)?><?= $uid ?>">
                    <thead>

                    <?php if(!$tableOptions['compact']){ ?>


                    <tr class="replace-inputs">
                        <?php if (isset($tableOptions['draggable'])) { ?>
                            <th class="table-headerField table-dataField" disabled></th>
                        <?php } ?>
                        <?php if (isset($tableOptions['bulk'])) { ?>
                            <th class="table-headerField table-dataField" disabled></th>
                        <?php } ?>
                        <?php foreach ($tableColumns as $columnData) { ?>
                            <?php if(!isset($columnData['format']) || $columnData['format'] != 'hidden'){ ?>
                                <th class="table-headerField table-dataField"></th>
                            <?php } ?>
                        <?php } ?>
                        <?php if (($tableIndividuals[0])) { ?>
                            <th class="table-headerField table-dataField" style="display:none;"></th>
                        <?php } ?>
                    </tr>

                    <tr id="table-headingsRow">
                        <?php if (isset($tableOptions['draggable'])) { ?>
                            <th class="table-headerField table-dataField">Order</th>
                        <?php } ?>
                        <?php if (isset($tableOptions['bulk'])) { ?>
                            <th class="table-headerField table-dataField"><input autocomplete="off" type="checkbox" checked="checked" disabled="disabled">
                            </th>
                        <?php } ?>
                        <?php foreach ($tableColumns as $columnData) { ?>
                            <?php if(!isset($columnData['format']) || $columnData['format'] != 'hidden'){ ?>
                                <th class="table-headerField table-dataField"><?= $columnData['display'] ?></th>
                            <?php } ?>
                        <?php } ?>
                        <?php if (($tableIndividuals[0])) { ?>
                            <th class="table-headerField table-dataField">Actions</th>
                        <?php } ?>
                    </tr>



                    <?php }else{ ?>
                        <tr>
                            <?php if (isset($tableOptions['draggable'])) { ?>
                                <th style="display: none;"></th>
                            <?php } ?>
                            <?php if (isset($tableOptions['bulk'])) { ?>
                                <th style="display: none;"></th>
                            <?php } ?>
                            <?php foreach ($tableColumns as $columnData) { ?>
                                <?php if(!isset($columnData['format']) || $columnData['format'] != 'hidden'){ ?>
                                    <th style="display: none;"></th>
                                <?php } ?>
                            <?php } ?>
                            <?php if (($tableIndividuals[0])) { ?>
                                <th style="display: none;"></th>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    </thead>
                    <?php
                    if (isset($tableOptions['draggable'])) {
                        $dragClass = 'class="draggablePanelList"';
                    } else {
                        $dragClass = "";
                    }
                    ?>

                    <tbody id="table-dataRows" <?= $dragClass ?>>


                    <?php
                    $i = 0;
                    foreach ($tableData as $key => $tableRow) {
                        $i++;

                        ?>
                        <?php
                        if (isset($tableOptions['customItemID'])) {
                            if (stristr($tableOptions['customItemID'], "arg:")) {
                                $CID = 'id="' . $tableRow[substr($tableOptions['customItemID'], 4)] . '"';
                            } else {
                                $CID = 'id="' . $key . '"';
                            }
                        } else {
                            if ($tableRow['ID']) {
                                $CID = $tableRow['ID'];
                            } else {
                                $CID = "";
                            }
                        }
                        ?>
                        <tr class="table-dataRow <?= ($i % 2 == 0) ? 'even' : 'odd'; ?>" <?= $CID ?>>
                            <?php if (isset($tableOptions['draggable'])) { ?>
                                <td class="table-dataField table-draggableTab"
                                    style=" cursor:n-resize; width:20px; height: 20px; background-color:#f9f2f4;">
                                    &#10070;</td>
                            <?php } ?>

                            <?php if (isset($tableOptions['bulk'])) { ?>
                                <td class="table-checkBox table-dataField"><input autocomplete="off" type="checkbox" data-attr="<?= $CID ?>" onclick="toggleTicks<?= $uid ?>(this)">
                                </td>
                            <?php } ?>

                            <?php foreach ($tableColumns as $columnData) {
                                foreach ($tableRow as $field => $value) {
                                    if ($field == $columnData['colname']) {
                                        if(isset($columnData['format'])){
                                            switch($columnData['format']){
                                                case "checkbox": ?>
                                                    <td class="table-dataField">
                                                        <input
                                                            type="checkbox"
                                                            <?=($value)?"checked":""?>
                                                            <?=($columnData['disabled'])?"disabled":""?>
                                                            name="<?="TableData-".$tableRow['ID']."-".$columnData['colname']?>"
                                                        />
                                                    </td>
                                                    <?php break;
                                                case "hidden": ?>
                                                    <input
                                                        type="hidden"
                                                        name="<?="TableData-".$tableRow['ID']."-".$columnData['fieldname']."-".$columnData['colname']?>"
                                                    /> <?php break;
                                            }
                                         }else{ ?>
                                           <td class="table-dataField"><?=$value?></td>
                                        <?php } ?>
                                    <?php }
                                }
                            } ?>
                            <?php if ($tableIndividuals[0]) { ?>
                                <td>
                                    <?php foreach ($tableIndividuals as $indiOpt) {
                                        $args = array();
                                        foreach ($indiOpt['action']['args'] as $argument) {
                                            if ($indiOpt['action']['function'] == "Main_Page_NAVIGATE") {
                                                $args[] = processArgsIntoString($argument, $tableRow, false);
                                            } else {
                                                $args[] = processArgsIntoString($argument, $tableRow, true);
                                            }
                                        }

                                        $args = implode(", ", $args);
                                        ?>

                                        <a class="table-dataField table-individualOption btn btn-<?= $buttons[$indiOpt['text']]['type'] ?> btn-sm btn-icon icon-left"
                                           onclick="<?= $indiOpt['action']['function']; ?>(<?= $args ?>);"><?= $indiOpt['text'] ?>
                                            <i class="entypo-<?= $buttons[$indiOpt['text']]['icon'] ?>"></i></a>
                                    <?php
                                    } ?> </td>
                            <?php } ?>
                        </tr>

                    <?php } ?>
                    </tbody>
                    <?php if(!$tableOptions['compact']){ ?>
                        <tfoot>
                        <tr>
                            <?php if (isset($tableOptions['draggable'])) { ?>
                                <th class="table-headerField table-dataField">Order</th>
                            <?php } ?>
                            <?php if (isset($tableOptions['bulk'])) { ?>
                                <th class="table-headerField table-dataField" style="width:20px"><input autocomplete="off" type="checkbox" checked="checked" disabled="disabled">
                                </th>
                            <?php } ?>
                            <?php foreach ($tableColumns as $columnData) { ?>
                                <?php if(!isset($columnData['format']) || $columnData['format'] != 'hidden'){ ?>
                                    <th class="table-headerField table-dataField"><?= $columnData['display'] ?></th>
                                <?php } ?>
                            <?php } ?>
                            <?php if (($tableIndividuals[0])) { ?>
                                <th class="table-headerField table-dataField" width="220">Actions</th>
                            <?php } ?>
                        </tr>
                        </tfoot>
                    <?php } ?>
                </table>

            </div>

        </div>

    </div>
<?php } ?>