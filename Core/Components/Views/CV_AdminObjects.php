<?php

class CV_AdminObjects {
    public static function newAdminForm(){

        $beginForm = "<form method='POST' action='admin.php?zone=4&action=New+Admin+Make'>";
        $endForm = "</form>";

        $button .= '<input autocomplete="off" class="btn btn-success" type="submit" name="newAdmin" value="Create New Admin"/>';




          echo '<div id="new-admin-box">';
          echo $beginForm;
          echo "<label>Email Address</label><input autocomplete='off' type='text' placeholder='Email Address' name='newAdminEmail' /><br/><label>Password</label><input autocomplete='off' type='password' placeholder='Password' name='newAdminPassword' /><br/>";

          echo $button;
          echo $endForm;
          echo '<button class="btn btn-danger" onClick="window.location.href=\'?zone=4\'">Cancel</button>';
          echo '</div>';

    }


     public static function newAccountForm(){

        $beginForm = "<form method='POST' action='admin.php?zone=2&action=New+Account+Make'>";
        $endForm = "</form>";

        $button .= '<input autocomplete="off" class="btn btn-success" type="submit" name="newAccount" value="Create New Account"/>';




          echo '<div id="new-Account-box">';
          echo $beginForm;
          echo '<label>Email Address</label><input autocomplete="off" type="text" placeholder="Email Address" name="newAccountEmail" /><br/><label>Password</label><input autocomplete="off" type="password" placeholder="Password" name="newAccountPassword" /><br/>';

          echo $button;
          echo $endForm;
          echo '<button class="btn btn-danger" onClick="window.location.href=\'?zone=2\'">Cancel</button>';
          echo '</div>';

    }

    public static function drawAdminTable(){
        $ha[0] = "ID";
        $ha[1] = "email";
        $ha[2] = "password";

        $hw[0] = 30;
        $hw[1] = 200;
        $hw[2] = 450;

        $actions[0] = "New Admin";
        $actions[1] = "Edit Admin";
        $actions[2] = "Delete Admin";

        Table::drawTable("admin", $ha, $ha, $hw, $actions);
    }

        public static function drawPDFTable(){

        $ha[0] = "PDFLocation";
        $ha[1] = "Uploaded";
        $ha[2] = "Datestamp";
        $ha[3] = "ViewDownloadCt";
        $ha[4] = "AssociatedAccount";
        $ha[5] = "Visible";

        $ta[0] = "Title";
        $ta[1] = "Date & Time Uploaded";
        $ta[2] = "Date & Time of Document";
        $ta[3] = "View Count";
        $ta[4] = "Account that Owns";
        $ta[5] = "Visible";

        $hw[0] = 100;
        $hw[1] = 180;
        $hw[2] = 180;
        $hw[3] = 100;
        $hw[4] = 70;
        $hw[5] = 50;

        $actions[0] = "View Document";
        $actions[1] = "Switch Visible";
        $actions[2] = "Upload PDF";


        Table::drawTable("Document", $ha, $ta, $hw, $actions);
    }

    public static function editAdminForm(){

        $adminEdit = new Admin("admin", $_GET['itemID']);
        +

        $beginForm = "<form method='POST' action='admin.php?zone=4&action=Edit+Admin+Apply&itemID=".$_GET['itemID']."'>";
        $endForm = "</form>";

        $button .= '<input autocomplete="off" type="submit" name="newAdmin" value="Edit this Admin"/>';

          echo '<div id="new-admin-box">';
          echo $beginForm;
          echo '<label>Email Address</label><input autocomplete="off" type="text" placeholder="Email Address" name="newAdminEmail"/><label>Password</label><input autocomplete="off" type="password" placeholder="New Password" name="newAdminPassword" /><br/>';

          echo $button;
          echo $endForm;
          echo '<button class="btn btn-danger" onClick="window.location.href=\'?zone=4\'">Cancel</button>';
          echo '</div>';

    }

    public static function accountTable(){
        $ha[0] = "ID";
        $ha[1] = "Email";
        $ha[2] = "isChildOf";
        $ha[3] = "Active";

        $ht[0] = "ID";
        $ht[1] = "Email";
        $ht[2] = "Child of Account";
        $ht[3] = "Account Active";

        $hw[0] = 50;
        $hw[1] = 200;
        $hw[2] = 200;
        $hw[3] = 100;

        $actions[0] = "Edit Account";
        $actions[1] = "New Account";
        $actions[2] = "Delete Account";
        $actions[3] = "Switch Active";
        $actions[4] = "Set Parent";
        $actions[5] = "Upload PDF to";


        Table::drawTable("Account", $ha, $ht, $hw, $actions);
    }


    public static function PDFUploadForm($accountID = null){

     $printString .='<form action="admin.php?zone=3&action=Process+Upload" method="post"   enctype="multipart/form-data">
        <label for="file">File:</label>
        <input autocomplete="off" type="file" name="file[]" id="file" multiple="">';

        if(!$accountID){
            $IDS = $GLOBALS['DBO']->getIDs("Account");
            $printString .='<select name="itemID">';
            $printString .='<option value="Multi">Multiple Accounts</option>';
            foreach($IDS as $ID){
                $account = new Account("admin", $ID);
                $email = $account->getEmail();
                $printString .='<option value="'.$ID.'">('.$ID.")->".$email.'</option>';
            }
            $printString .='</select>';

        }else{
            $printString .='<input autocomplete="off" type="hidden" name="itemID" value="'.$accountID.'">';
        }

        $printString .='<input autocomplete="off" type="submit" class="btn btn-success" name="submit" value="Submit"></form>';
        echo $printString;

}

    public static function SetParentForm(){

     $printString .='<form action="admin.php?zone=2&action=Action+Parent" method="post">     ';
            $IDS = $GLOBALS['DBO']->getIDs("Account");
            $printString .='<select name="itemID">';
            $printString .='<option value="NULL">None</option>';

            foreach($IDS as $ID){
                $account = new Account("admin", $ID);
                $email = $account->getEmail();
                $printString .='<option value="'.$ID.'">('.$ID.")->".$email.'</option>';
            }
            $printString .='</select>';
         $printString .='<input autocomplete="off" type="hidden" name="childID" value="'.$_GET['itemID'].'">';
        $printString .='<input autocomplete="off" type="submit" class="btn btn-success" name="submit" value="Submit"></form>';
        echo $printString;

}



}
