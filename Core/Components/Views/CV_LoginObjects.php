<?php

class CV_LoginObjects {
    public static function loginForm($id,$method,$fields,$keepValues = false, $message = '',$forgotten = false,$target = ""){
        
        $beginForm = "";
        $messageBox = "";
        $formFields = "";
        $button = "";
        $endForm = "";
        switch ($method){

               case "POST":
                   $beginForm = "<form method='POST' autocomplete='off' target='".$target."' action='' id='".$id."-login-form' class='navbar-form navbar-right ps' role='search'> <strong>LOGIN</strong> ";
                   $endForm ="</form>";
                   $button = '<button type="button" id="'.$id.'-login-button" class="btn btn-default" name="login" value="Login" onclick="loginTo(this)">></button><div id="captchaFL"> <div id="captchaAREA"></div><div style="width:30px; display:inline-block;"><div class="btn btn-warning" onclick="retryLogin(this)" style="height:76px; line-height:16px; text vertical-align:top; display:inline-block;"><br/>Validate +<br/>  Try again</div></div></div>'."</div>";
                   
                   if($forgotten){
                       '<input autocomplete="off" type="submit" id="'.$id.'-login-forgot-button" class="btn btn-default" name="forgot" value="Forgotten Details"/>';
                   }
               
                   break;
               case "AJAX":
                   $beginForm = "<form method='POST' action=''>";
                   $endForm ="</form>";
                   $button = '<input autocomplete="off" type="submit" id="'.$id.'-login-button" name="login" class="btn btn-success" value="Login"/>';
                   
                     if($forgotten){
                       $button .= '<input autocomplete="off" type="submit" id="'.$id.'-login-forgot-button" class="btn btn-success" name="forgot" value="Forgotten Details"/>';
                   }
                   break;
        
        }

        $message .= ' | <a style="color:#ffffff;" href="/pending">Forgot Password?</a>';

        if($message){
            $colourClass = getMessageColourClass($message);
            //print_r($colourClass);
            //print_r($message);
            $messageBox = '<div class="'.$colourClass.'" id="'.$id.'-login-message">'.$message.'</div>';
        
        }
        foreach($fields as $key => $field){



            if(StringFunctions::stringContains($field[0], "pass")){
                $type = "password";
            }else if(StringFunctions::stringContains($field[0], "email")){
                $type = "email";
            }else{
                $type = "text";
            }
            if(isset($field[1])){

                if($keepValues && !StringFunctions::stringContains($field[0], "pass")){
                    $value = 'value="'.htmlentities($field[1]).'"';
                }else{
                    $value = "";
                }

            }else{
                $value = "";
            }

            if($key != sizeof($fields) -1){
                $ed = "</div> ";
            }else{
                $ed = "";
            }
            //$formFields .= '<label id="'.$id.'-login-'.$field[0].'-label'.'" class="'.$id.'-formLabel'.'">'.ucfirst($field[0]).'</label><br/>';
            $formFields .= '<div class="form-group"><input autocomplete="off" autocomplete="off" onkeydown="if (event.keyCode == 13) {loginTo(this);}" id="'.$id.'-login-'.$field[0].'-field'.'" class="form-control" class="'.$id.'-formField'.'" type="'.$type.'" name="'.$id.'-login-'.$field[0].'-field'.'" placeholder="'.ucfirst($field[0]).'"'.$value.' />'.$ed;
            
        }
        
        
        $rs = "
        <script>
        $(function(){
            $('#" . $id . "-login-form').submit(function( event ){
                 if($('#" . $id . "-login-form').hasClass('ps')){
                    event.preventDefault();
                 }
             });
         });

        function loginTo(button){
            $('#User-login-button').html('<img height=\"20\" src=\"./Assets/img/core/bx_loader.gif\">');
            $('.form-control').attr('disabled','disabled');

            $('input[type=password]').each(function(id, obj){
                $(obj).val(Base64.encode($(obj).val()));
            });

            $.get('./Assets/AJAX/GNT.php',function(data){
               handleCcheck(data,button.parentNode.parentNode);
            });
        }


        function retryLogin(button){
            $('.form-control').removeAttr('disabled');

            var grs= grecaptcha.getResponse();

            $.get('./Assets/AJAX/GNT.php?retry='+grecaptcha.getResponse(),function(data){
                  handleCcheck(data,button.parentNode.parentNode.parentNode.parentNode);
            });
        }

        function handleCcheck(data, form){
         switch(data){
                    case \"OK\":
                        $('.form-control').removeAttr('disabled');
                        $(form).removeClass('ps');
                        $(form).submit();
                        break;
                    case \"VC\":
                        $(form).addClass('ps');
                        $('#captchaFL').slideDown(100);
                        break;
                    case \"BA\":
                        $(form).addClass('ps');
                        $('#captchaFL').slideDown(100);
                        $('#captchaFL').html(\"Please wait a while and try again.\");
                        break;
                    default:
                        $(form).addClass('ps');
                        $('#captchaFL').slideDown(100);
                        $('#captchaFL').html(\"An Error Occurred. Please refresh.\");
                        break;
                }
        }
        </script>";

          //$rs.= '<div id="'.$id.'-login-box">';
          $rs.= $beginForm;
          $rs.= $formFields;
          $rs.= $button;
          $rs.= $endForm."<br/><br/><br/>".$messageBox;
          //$rs.= '</div>';
          return $rs;
    }
    
    public static function logoutForm($id,$target = ""){        
        $beginForm = "<form style='' method='POST' target='".$target."' action='' id='".$id."-logout-form'>";
        $endForm ="</form>";
        $button = '<li><button type="submit" class="btn btn-default" id="'.$id.'-logout-button" name="'.$id.'" value="Logout"> Logout <i class="entypo-logout right"></i> </button></li>';
        $shadowButton = '<li><input autocomplete="off" type="button" class="btn btn-default" onclick="doAct(\'D_SM_IU\',\'\');" value="End Impersonation" /></li>';

        $rs = "";
          
        $rs.= '';
        $rs.= $beginForm;
        $rs.=  '';
        if($_GET['controller'] != "dashboard"){
        //    $rs .= '<a href="dashboard" style="border-radius: 4px 0px 0px 4px;" class="btn btn-info" class="form-control">Dashboard</a>';
        }else{
         //   $rs .= '<a href="home" style="border-radius: 4px 0px 0px 4px;" class="btn btn-info" class="form-control">Front Site</a>';
        }
        if(isset($_SESSION['shadowUser'])){
            $rs .= $shadowButton;
        }else{
           $rs.= $button;
        }

        $rs .="";

        $rs.= $endForm;
        $rs.= '';
        return $rs;
    }

}

?>
