<?php

class CV_EditObjects {

   public static function echoErrors($errors){
       return '<div>'.$errors.'<div/>';
   }

    public static function echoEditForm($obj,$fields, $type = 1){
        switch($type){
            case 3:
                $_SESSION['editableO'][] = $obj;

                $finalString .= "<div class='wholeFormContainer'>";
                 $finalString .= "<h3> Edit ".get_class($obj)."</h3>";
                  $finalString .= "<div id='updatingGIF".get_class($obj)."' class='updatingGIF' style='z-index:50; width:200px; height:200px; background-color:#fff; box-shadow:0px 5px 5px; border-radius:5px; text-align:center; padding-top:40px; position:absolute; left:calc(50% - 100px); display:none;'><img src='http://".$_SERVER['SERVER_NAME']."/images/spinner.gif'/><br/><br/> Updating ...</div>";
                 $finalString .= "<div id='editForm".get_class($obj)."' style='z-index:-50;'>";
                 $finalString .= '
                                 <script type=\'text/javascript\'>
				$(document).ready(function(){
					$("#UPDV'.get_class($obj).'").click(function(){
                                            $("#updatingGIF'.get_class($obj).'").fadeIn(400, function(){
                                                $("#editForm'.get_class($obj).'").animate({opacity: 0.4}, 400);
                                            });
				            var FORMDATA = $("form").serialize();


				            '.generateAJAX("get", "../AJAX/Method3Update.php", "{'fd' : FORMDATA}", 'updateData').'
					});

                                        function updateData(data,other){

                                            $("#updatingGIF'.get_class($obj).'").delay(1000).fadeOut(400, function(){
                                                $("#editForm'.get_class($obj).'").animate({opacity: 1.0}, 400);
                                            });

                                        }

                                });
                                ';
                 $finalString .= "</script>";

                 $finalString .= "<form id='edit".get_class($obj)."' name='edit".get_class($obj)."a' method='get' action=''>";




                foreach($fields as $field){
                    $methodName = "get".$field;
	            if(method_exists($obj, $methodName)){
	                     $currentValue = $obj->$methodName();


	                     $finalString .= "<div class='editRow'>";
	                        $finalString .= "<div id='editTitle-".$field."'>";
	                           $finalString .= $field;
	                        $finalString .= "</div>";
	                        $finalString .= "<div id='editForm-".$field."' style=''>";
                                    if(stristr($field, "country")){
                                        $finalString .= '<select name="'.$field.'"> ';
                                        $countries = $GLOBALS['DBO']->retrieve('WCountries');
                                        foreach($countries as $country){
                                            if($currentValue == $country['Country Name']){
                                                $selectedVal = 'selected="selected"';
                                            }else{
                                                unset($selectedVal);
                                            }
                                            $finalString .= '<option '.$selectedVal.' value="'.$country['Country Name'].'">'.$country['Country Name'].'</option>';
                                        }
                                        $finalString .= '</select>';
                                    }else if(stristr($field, "password")){
                                        $finalString .= "<input autocomplete='off' type='password' placeholder='".$field."' name='".$field."' value='".$currentValue."'>";
                                    }else{
                                        $finalString .= "<input autocomplete='off' type='text' placeholder='".$field."' name='".$field."' value='".$currentValue."'>";
                                    }

	                        $finalString .= "</div>";
	                     $finalString .= "</div>";
                     }


                    }
                    $finalString .= '<br/><center><input autocomplete="off" type="button" class="btn btn-success" name="SNV" value="Update Values" id="UPDV'.get_class($obj).'"/></center>';
                $finalString .= "</form></div></div>";
                return $finalString;
                break;
            case 2:
                if(!$_SESSION['editableO']){
                    $_SESSION['editableO'] = $obj;
                }

                 $finalString .= "<div id='editForm'>";

                foreach($fields as $field){
                    $methodName = "get".$field;
                    if(method_exists($obj, $methodName)){
                        $currentValue = $obj->$methodName();
                    }else{
                        $currentValue = "";
                    }



                       $finalString .= "<script type='text/javascript'>";
                       $finalString .= "function edit".$field."(){"
                               . " document.getElementById('editForm-".$field."').style.display = 'block';  "
                               . " document.getElementById('viewContent-".$field."').style.display = 'none';"
                               . "}"
                               . ""
                               . "function update".$field."(){"
                                . "var value = $('input[name=".$field."]').val();"
                                . generateAJAX("get", "../AJAX/HandleAdminUpdate.php", "{'data' : value , 'field' : '".$field."'}", "gmrv".$field)
                               . "}"
                               . ""
                               . "function gmrv".$field."(data, other){"
                                    . "$('input[name=".$field."]').val(data);"
                                    . "$('#static".$field."').html(data);"
                                    . " document.getElementById('editForm-".$field."').style.display = 'none';  "
                                    . " document.getElementById('viewContent-".$field."').style.display = 'block';"
                                    . "searchStuff();"
                               . "}"
                               ;
                       $finalString .= "</script>";


                     $finalString .= "<div class='editRow'>";
                        $finalString .= "<div id='editTitle-".$field."'>";
                           $finalString .= $field;
                        $finalString .= "</div>";
                        $finalString .= "<div id='viewContent-".$field."'>";
                           $finalString .= '<p id="static'.$field.'">'.$currentValue.'</p>';
                           $finalString .= '<input autocomplete="off" type="button" onclick="edit'.$field.'();"  value="Edit"/>';
                        $finalString .= "</div>";

                        $finalString .= "<div id='editForm-".$field."' style='display:none;'>";
                                $finalString .= "<input autocomplete='off' type='text' placeholder='".$field."' name='".$field."' value='".$currentValue."'>";
                                $finalString .= "<br/><center><input autocomplete='off' onclick='update".$field."();' type='button' name ='editable-".get_class($obj)."' value='Update' /></center>";
                        $finalString .= "</div>";




                    //GRAB CURRENT VALUES
                    //PRINT OUT VALUES + TITLES + SHOW BUTTON
                    //PRINT FORM + BUTTON
                    //JSFUCNTION
                     $finalString .= "</div>";
                    }
                $finalString .= "</div>";
                return $finalString;
                break;
            default:
                $finalString .= "<div id='editForm'>";
                foreach($fields as $field){
                    $methodName = "get".$field;
                    if(method_exists($obj, $methodName)){
                        $currentValue = $obj->$methodName();
                    }else{
                        $currentValue = "";
                    }


                       $finalString .= "<script type='text/javascript'>";
                       $finalString .= "function edit".$field."(){"
                               . " document.getElementById('editForm-".$field."').style.display = 'block';  "
                               . " document.getElementById('viewContent-".$field."').style.display = 'none';"
                               . "}";
                       $finalString .= "</script>";


                     $finalString .= "<div class='editRow'>";
                        $finalString .= "<div id='editTitle-".$field."'>";
                           $finalString .= $field;
                        $finalString .= "</div>";
                        $finalString .= "<div id='viewContent-".$field."'>";
                           $finalString .= $currentValue;
                           $finalString .= '<input autocomplete="off" type="button" onclick="edit'.$field.'();"  value="Edit"/>';
                        $finalString .= "</div>";

                        $finalString .= "<div id='editForm-".$field."' style='display:none;'>";
                            $finalString .= "<form id='Form-".$field."' method='POST' target=''>";
                                $finalString .= "<input autocomplete='off' type='text' placeholder='".$field."' name='".$field."' value='".$currentValue."'>";
                                $finalString .= "<input autocomplete='off' onclick='update".$field."();' type='submit' name ='editable-".get_class($obj)."' value='Update' />";
                            $finalString .= "</form>";
                        $finalString .= "</div>";




                    //GRAB CURRENT VALUES
                    //PRINT OUT VALUES + TITLES + SHOW BUTTON
                    //PRINT FORM + BUTTON
                    //JSFUCNTION
                     $finalString .= "</div>";
                    }
                $finalString .= "</div>";
                return $finalString;
                break;
        }
       //TYPE 1 -> each box has its own edit button.
       //TYPE 2 -? One big form with an edit button at the bottom.

   }
}
