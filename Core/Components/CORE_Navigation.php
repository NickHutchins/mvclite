<?php
    /**
     * Description of Login
     *
     * @author Nick
     */
    class CORE_Navigation {

        var $links;
        var $footLinks;
        var $allowedNavs;
        var $pageName;

        public function __construct($publicLinks = ALLOWEDNAVS, $startingLinks = array(), $footLinks = array(), $bypassLogged = false) {
            $this->links = $startingLinks;

            //checks if the logged in accounts the users is logged into remove or add links.



            if (isset($_SESSION['logged']) && is_array($_SESSION['logged']) && !$bypassLogged) {
                foreach ($_SESSION['logged'] as $class) {
                    if ($class instanceof navAreas) {
                        $this->addLinks($class->addsLinks());
                    }
                }

                foreach ($_SESSION['logged'] as $class) {
                    if ($class instanceof navAreas) {
                        $this->removeLinks($class->removeLinks());

                    }
                }

                foreach ($_SESSION['logged'] as $class) {
                    if ($class instanceof navAreas) {
                        $this->allowedNavs = $class->allowedNavs();
                    }
                }
                foreach ($_SESSION['logged'] as $class) {
                    if ($class instanceof navAreas) {
                        //DISABLED
                    }
                }
            }

            if ($this->allowedNavs) {
                $this->allowedNavs = array_merge($this->allowedNavs, $publicLinks);
            } else {
                $this->allowedNavs = $publicLinks;
            }


            $this->links = array_unique($this->links);
        }

        private function addLinks($linkArray) {
            foreach ($linkArray as $key => $linkText) {
                if (!is_numeric($key)) {
                    $this->links[] = $linkText;
                } else {
                    $this->links[$key] = $linkText;
                }
            }
        }

        private function removeLinks($linkArray) {
            foreach ($linkArray as $key => $linkText) {
                if (in_array($linkText, $this->links)) {
                    unset($this->links[array_search($linkText, $this->links)]);
                }
            }
        }

        private function getHeaderLinks($method = "return") {
            $rs = "";
            foreach ($this->links as $key => $link) {
                if (!is_numeric($key)) {
                    $rs.= " | <a href='" . $key . "'>" . ucfirst(strtolower($link)) ."</a>";
                } else {
                    $rs.= " | <a href='//" . $_SERVER['SERVER_NAME'] . "/" . ucfirst(strtolower($link)) . "'>" . ucfirst(strtolower($link)) . "</a>";
                }
            }
            return substr($rs, 2);
        }

        private function getFooterLinks() {}

        private function getAllowedNavs(){
            return $this->allowedNavs;
        }

        private function isAllowedNav($nav){
            return in_array($nav, $this->allowedNavs);
        }

        public static function isAllowed($object, $link, $otherAllowedLinks){
            if ($object instanceof navAreas) {
                $navList = array_merge($object->allowedNavs(),$otherAllowedLinks);
                foreach($object->disabledNavs() as $link2){
                    $navList = CORE_Navigation::removeFrom($navList, $link2);
                }
                return (in_array($link, $navList));
            }else{
                return (in_array($link, $otherAllowedLinks));
            }

        }

        public static function removeFrom($array,$remove){
            foreach ($array as $key => $page){
                if($page == $remove){
                    unset ($array[$key]);
                }
            }
            return $array;
        }


        public function get_output() {

            $this->convertControllerAndViewToLowercase();


            if ($_GET['controller'] == "index.php" || $_GET['controller'] == "") {
                $_GET['controller'] = 'home';
            }

            $controllerName = $this->check_controller($_GET['controller']);
            $nameSpacedControllerName = "\\Controller\\".$controllerName;

            $controllerObject =  new $nameSpacedControllerName();

            $viewName =  $this->check_controller_function($controllerObject, $controllerName);

            $controllerFunction = "view_".$viewName;

            $controllerData = $controllerObject->$controllerFunction();

            return $this->run_view($controllerName, $viewName, $controllerData);
        }

        private function convertControllerAndViewToLowercase()
        {
            $_GET['controller'] = strtolower($_GET['controller'] ?? "");
            $_GET['view'] = strtolower($_GET['view'] ?? "");
        }

        private function check_controller($className){
            if ($className != "") {

                $nameSpacedClassName = "\\Controller\\".$className;

                if (class_exists($nameSpacedClassName)) {
                    if (method_exists($nameSpacedClassName, "view_main")) {
                        if (in_array(strtoupper($className), $this->allowedNavs)) {
                            return $className;
                        }else{
                            $this->dieToError(401);
                        }
                    }
                }
            }
            $this->dieToError(404);
        }

        private function check_controller_function($controllerObject , $controllerName){

            if($_GET['view']){

                if (method_exists($controllerObject, "view_".$_GET['view'])){
                    $this->pageName = $controllerName." - ".$_GET['view'];
                    return $_GET['view'];
                }else{
                    if($controllerObject->handles_own_nav()){
                        $this->pageName = $controllerName;
                        return "main";
                    }

                    $this->dieToError(404);
                }
            }else{
                $this->pageName = $controllerName;
                return "main";
            }
        }

        private function run_view($page, $screen, $pageData){

            $this->setPageHeaders($pageData['header']['code']);

            if($pageData['customredirect']){
                $this->dieToCustomRedirect($pageData['customredirect'], $pageData['header']['code']);
            }

            if($pageData['redirect']){
                $this->dieToRedirect($pageData['redirect'], $pageData['header']['code']);
            }

            if($pageData['response'] != "" && $pageData['type'] == "API"){
                return json_encode($pageData['response']);
            }

            $viewFile = 'View/Views/'.$page.'/'.$screen.'.php';
            $innerCode = View::storeRequireIntoText($viewFile, $pageData);
            unset($pageData['viewBag']); // We are done with the viewBag now!

            $templateName = $this->work_out_template($pageData['template']);
            unset($pageData['template']); //We are done with the template too (if there was one!)

            $pageResult = $this->check_render_template($templateName, $pageData, $innerCode);

            if($pageResult != ""){
                return $pageResult;
            }else{
                throw new EXCEPTIONCLASS("No View File", "You have a controller function for this page, but no view to back it up! - Needs to be called->".$viewFile, "Core Error");
            }
        }

        private function setPageHeaders($headerCode = 201)
        {
            http_response_code($headerCode);
        }

        private function work_out_template($template)
        {
            if($template === ""){
                return "";
            }

            return $template?:"main";
        }

        private function check_render_template($templateName, $pageData, $renderedCode)
        {

            $templateString = "View/Templates/".$templateName.".php";
            if(file_exists($templateString))
            {
                $pageData['existingText'] = $renderedCode;
                return View::storeRequireIntoText($templateString, $pageData);
            }
            else
            {
                return $renderedCode;
            }

        }

        private function dieToError($type = 404){
            $this->setPageHeaders($type);
            die(header('Location:  //'.SiteSetting::get('homeDomain').'/error/'.$type));
        }

        private function dieToRedirect($url, $code = 301){
            $this->setPageHeaders($code);
            die(header('Location:  //'.SiteSetting::get('homeDomain').'/'.$url));
        }


        private function dieToCustomRedirect($url, $code = 301){
            $this->setPageHeaders($code);
            die(header('Location: '.$url));
        }

        public function getPageTitle() {
            return $this->pageName;
        }

    }
