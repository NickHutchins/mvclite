<?php

/**
 * Description of Login
 *
 * @author Nick
 */
class CORE_Login {

    var $fieldList;
    var $logInto;
    var $loginObject;
    var $dispForms;
     
    public function __construct($logInto, $dispForms = true){
       
       if(class_exists($logInto)){
           $demoAdmin[0] = "demo"; $demoAdmin[1] = "demo";
           $this->loginObject = new $logInto("temporary",$demoAdmin);
           if($this->loginObject instanceof loginAble){
              $this->fieldList = $this->loginObject->requiredVars();
              if($this->fieldList){
                foreach($this->fieldList as $loginVar){
                    if(!method_exists($this->loginObject, "validate".$loginVar)){
                         throw new Exception("Unable to Create new Login -> ".$logInto." is not a valid login object.");
                    }
                }
              }
              //WE HAVE OUR LOGIN OBJECT OKAY.
          }
       }
       $this->logInto = $logInto;
       if($dispForms){
            $this->dispForms = true;
        }else{
            $this->dispForms = false;
        }
    }
    
    /* 
     *Runs the login form.
     * -> If the login is okay, it will take the user to the new location (if applicable)
     * -> If not returns to same page with errors ();
     */
    public function run($values){
        if(!isset($_SESSION['logged'][$this->logInto])){
            if(isset($_SESSION['loginForm'][$this->logInto])){
                 unset($values['login']);
                 $values = StringFunctions::removeEmpties($values);

                if(StringFunctions::findInArrayValues(array_keys($values), $this->logInto)){
                $valid = $this->validateLogin($values);

                    if($valid === true){

                       $_SESSION['loginForm'][$this->logInto] = false;
                       $_SESSION['logged'][$this->logInto] = $this->loginObject;
                        @$_SESSION['logged'][$this->logInto]->onLogin();

                       $_GET['controller'] = $_SESSION['logged'][$this->logInto]->loggedInAreaPage();

                       if($this->dispForms){
                           CV_LoginObjects::logoutForm($this->logInto);
                       }

                    }else{
                       $this->failedLogin($values, false ,$valid);   
                       return $valid;
                    }
                }else{
                    $_SESSION['loginForm'][$this->logInto] = true;
                    $this->createNewLogin(false);
                }
            }else{
                $_SESSION['loginForm'][$this->logInto] = true;
                $this->createNewLogin(false);
            }
        }else{

            $newInstance = $_SESSION['logged'][$this->logInto]->update();

            if(is_object($newInstance)){
                $_SESSION['logged'][$this->logInto] =  $newInstance;
            }else{
                $_SESSION['logged'][$this->logInto]->onLogout();
                $_GET['controller'] = $_SESSION['logged'][$this->logInto]->loggedOutAreaPage();

                $_SESSION['logged'][$this->logInto]->logout();
                unset($_SESSION['logged'][$this->logInto]);
                unset($_SESSION['logged']);
                unset($_SESSION['shadownUser']);
                unset($_SESSION['su']);

                $this->run(array());
                return;
        }

            if(isset($values[$this->logInto]) && $values[$this->logInto] == "Logout"){
                $_SESSION['logged'][$this->logInto]->onLogout();
                $_GET['controller'] = $_SESSION['logged'][$this->logInto]->loggedOutAreaPage();

                $_SESSION['logged'][$this->logInto]->logout();
                unset($_SESSION['logged'][$this->logInto]);
                if(sizeof($_SESSION['logged']) == 0){
                    unset($_SESSION['logged']);
                }

                $this->run(array());

            }else{
                if($this->dispForms){
                   echo CV_LoginObjects::logoutForm($this->logInto);
                }
            }

            
        }
    }
    
    public function isLoggedin(){
        if($_SESSION['logged'][$this->logInto]){
            return true;
        }else{
            return false;
        }

    }
    
    private function validateLogin($values){

        if(!isset($_SESSION['blocker'])){
            return "Its not going to be that easy!";
        }


        if($_SESSION['blocker'] == true){
            unset($_SESSION['blocker']);
            return "Closer, but still not quite right.";
        }



        unset($_SESSION['blocker']);

        //Create Actual Object for use
        foreach($values as $key => $val){
            if($val == ""){
                unset($values[$key]);
            }
        }


        if(sizeof($values) == 0){
            return false;
        }

        if(sizeof($values) < sizeof($this->fieldList)){
             return "Not all fields filled in.";
        }



        //echo "Ok";

        $logValues = $values;
        $logValues[$this->logInto.'-login-Password-field'] = "XXX-HIDDEN-XXX";


        $this->loginObject = new $this->logInto("login",$values);
        //print_r($this->loginObject);
        $IPUser = new IP($_SERVER['REMOTE_ADDR']);
        $IPUser->adjustIP(SiteSetting::get('spamsecPOINTS-doingAnAction'));


        if(!$this->loginObject->isInit()){
            Log::securityLog("User Doesn't Exist", $logValues);
            return "Failed: Incorrect login details provided.";//false; // VERBOSE RRORS

        }

        if($this->loginObject->isLockedOut()){
            Log::securityLog("User has been locked out", $logValues);
            return "Failed: Account Lockout. Please wait for a little while";//false; // VERBOSE ERRORS
        }




        $i = 0;
        foreach($values as $value){            
            $methodName = "validate".$this->fieldList[$i];
            if(stristr($this->fieldList[$i],"pass")){
                $value = base64_decode($value);
            }


            if(!method_exists($this->loginObject,$methodName)){
                continue;
            }

            if(!$this->loginObject->$methodName($value)){
                Log::securityLog("Incorrect Password Failed Login", $this->loginObject->getSEMail(), null);
                $this->loginObject->incrementLockout();
                return "Failed: Incorrect login details provided."; //Details errors not provided, due to securityy
            }
            $i++;
        }


        if($this->loginObject->isSimultaneousLogin()){
            Log::securityLog("Multiple login of the same account.", $logValues);
            return "Failed: User is already logged in.";//false; // VERBOSE ERRORS
        }


        if($this->loginObject->isSuspended()){
            Log::securityLog("User tried to log in whilst suspended", $logValues);
            return "Failed: Account/Company Suspended, please contact support.";//false; // VERBOSE ERRORS
        }


        $IPUser->adjustIP(-SiteSetting::get('spamsecPOINTS-doingAnAction'));
        return true;       
    }
    
    private function createNewLogin($override, $errors = NULL){
        $passFields = "";

        foreach($this->fieldList as $field){
            $passFields[][0] = $field;
        }      
        
        
        if($this->dispForms){
            echo CV_LoginObjects::loginForm($this->logInto, "POST", $passFields, false, $errors);
        }else if($override){
            return CV_LoginObjects::loginForm($this->logInto, "POST", $passFields, false, $errors);
        }
        
    }
        
    private function failedLogin($triedVariables, $override, $errors = NULL){

        //print_r($triedVariables)." - ".$override;
        //print_r(debug_backtrace());
        //$passFields = null;
        
            foreach($this->fieldList as $field){
                $passFields[][0] = $field;
            }
           
            
            foreach($triedVariables as $key => $var){
                $keySplit = explode("-",$key);
                //print_r($keySplit);

                if(sizeof($keySplit) == 4){

                    foreach($passFields as $key2 => $fields){

                        if($fields[0] == $keySplit[2]){
                            $passFields[$key2][1] = $triedVariables[$key];
                        }
                    }

                }

            }
        
        
        if($this->dispForms){
            echo CV_LoginObjects::loginForm($this->logInto, "POST", $passFields, false ,$errors);
        }else if($override){
            return CV_LoginObjects::loginForm($this->logInto, "POST", $passFields, false ,$errors);
        }
    }
    
    private function manualLogoutForm(){
        return CV_LoginObjects::logoutForm($this->logInto);
    }
    
    private function manualLoginForm($errors){
        if(StringFunctions::findInArrayValues($_POST, "Logout")){
            return $this->createNewLogin(true, $errors);
        }else{
            return $this->failedLogin($_POST,true,$errors);
        }
        
    }
    
    public function contextSpecificForm($errors){
        if(isset($_SESSION['logged'][$this->logInto])){
            return $this->manualLogoutForm();
       }else{
            return $this->manualLoginForm($errors);
       }
    }
    
    
    
    
}

?>
