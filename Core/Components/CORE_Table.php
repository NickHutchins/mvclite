<?php

/**
 * Table View Class Construction
 *
 * @author Nick
 */
class CORE_Table {

    public $tableName;
    public $tableData;
    public $tableColumns;
    public $tableOptions;
    public $tableIndividuals;
    public $tableBulks;
    public $hideOptions;
    
    public function __construct($tableName, $tableData, $columns ,$individualActions = array() , $bulkActions = array(), $options = array(), $hideOptions = true)
    {
        $this->tableName = $tableName;
        $this->tableData = $tableData;
        $this->tableColumns = $columns;
        $this->tableOptions = $options;
        $this->tableIndividuals = $individualActions;
        $this->tableBulks = $bulkActions;
        $this->hideOptions = $hideOptions;

    }

    private function handleOptions($ops){

    }

    public function getTableCode(){
        return View::storeRequireIntoText("Core/Components/Views/CV_TableView.php", $this);
    }

}
