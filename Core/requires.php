<?php
    ini_set( 'session.cookie_secure', 1 );
    ini_set( 'session.cookie_httponly', 1 );
    date_default_timezone_set('Europe/London');
    header('Strict-Transport-Security: max-age=31536000; includeSubDomains');
    header('X-XSS-Protection: 1; mode=block');
    header('X-Content-Type-Options: nosniff');

    /* BEGIN AUTOLOAD - The @ Surpresses errors.*/
    spl_autoload_register(function($class) {

        $SwitchSlashes = str_replace("\\","/",$class);

        @include_once 'Abstracts/' . $class . '.php';
        @include_once 'Core/Functions/' . $class . '.php';
        @include_once 'Core/Components/' . $class . '.php' ;
        @include_once 'Core/Components/Views/' . $class . '.php' ;
        @include_once 'Core/ErrorReporting/' . $class . '.php' ;
        @include_once 'Core/DB/' . $class . '.php' ;
        @include_once $SwitchSlashes. '.php' ;
        @include_once "Core/ThirdParty/".$SwitchSlashes. '.php' ;


    });

    set_error_handler("ErrorHandler::HandlePHPError");
    register_shutdown_function('ErrorHandler::HandlePHPFatal');

    @include_once "vendor/autoload.php";
    /* BEGIN REGULAR REQUIRES */
    require_once 'config.php';
    include_once 'Core/ErrorReporting/'.EXCEPTIONCLASS.'.php';

    \CRUD::manualQuery("set time_zone = '+01:00';",false);


