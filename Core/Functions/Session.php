<?php

class Session {


    public static function sessionReStart(){


        session_name(SESSIONNAME);
        session_start();

        if($_SESSION['OLDID']){
            $sessdata = $_SESSION;
            session_id($_SESSION['OLDID']);
            session_destroy();
            session_start();
            $_SESSION = $sessdata;
        }



        if($_SESSION['TIME']){
            if($_SESSION['TIME'] + 30 < time()){
                $_SESSION['OLDID'] = session_id();
                $_SESSION['TIME'] = time();
                session_regenerate_id();
            }else{
                unset($_SESSION['OLDID']);
            }
        }else{
            $_SESSION['OLDID'] = session_id();
            $_SESSION['TIME'] = time();
            session_regenerate_id();
        }


    }

    private static function timeDiff($greaterTime, $lesserTime){
        return $greaterTime - $lesserTime;
    }

    public static function checkgenSession(){

        Session::sessionReStart();


        if(Session::LOU() && !isset($_SESSION['shadowUser'])){
            // This token is used by forms to prevent cross site forgery attempts
            if (!isset($_SESSION['chlSESSH']['fingerprint'])) {
                $_SESSION['chlSESSH']['fingerprint'] = Session::generateUniqueFingerPrint();
            }else{
                if ($_SESSION['chlSESSH']['fingerprint'] != Session::generateUniqueFingerPrint()) {
                    session_destroy();
                    die('[{"act":"stopRunning","data":"STOP"},{"act":"alert","data":"Don\'t steal cookies :( Buy your own! <br/> They\'ll taste much nicer when you eat them."}]');
                    //die("Don't steal cookies :( Buy your own! They'll taste much nicer when you eat them.");
                }
            }
        }

    }

    public static function generateUniqueFingerPrint(){

        $s['userID'] = Session::LOU()->getIID();
        $s['UA'] = $_SERVER['HTTP_USER_AGENT'];
        $s['RA'] = $_SERVER['REMOTE_ADDR'];
        $s['RP'] = $_SERVER['RP'];

        return md5(implode("~s~",$s));

    }

    public static function LOU(){
        $user = $_SESSION['logged']['User'];
        /* @var $user User */
        return $user;
    }
}