<?php

class View {
    public static function partialView($name, $viewBag = array()){
        return View::storeRequireIntoText('View/Partial/'.$name.'.php', $viewBag);
    }

    public static function storeRequireIntoText($fileToRequire, $inPageData = null){

        if(is_array($inPageData) || is_object($inPageData)){
            foreach($inPageData as $key => $data){
                $$key = $data;
            }
        }

        ob_start();
        if(!include $fileToRequire){
            $className = EXCEPTIONCLASS;
            throw new $className("File Produced No Output.","7");
        };

        if(is_array($inPageData) || is_object($inPageData)){
            foreach($inPageData as $key => $data){
                unset($$key);
            }
        }

        $var = ob_get_contents();
        ob_end_clean();
        return $var;
    }
}

