<?php

class StringFunctions
{
    public static function startsWith($haystack, $needle)
    {
        return !strncmp($haystack, $needle, strlen($needle));
    }

    public static function endsWith($haystack, $needle)
    {

        $needle = str_replace("\r", "\~@######", $needle);
        $needle = str_replace("~@######", "r", $needle);

        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }


        return (substr($haystack, -$length) == $needle);
    }

    public static function removeAllBut($what, $string)
    {
        switch ($what) {
            case "lowercase":
                return preg_replace("/[^a-z]/", "", $string);
            case "uppercase":
                return preg_replace("/[^A-Z]/", "", $string);
            case "numeric":
                return preg_replace("/[^0-9]/", "", $string);
            case "symbolic":
                return preg_replace("/[0-9a-zA-Z]/", "", $string);
            default:
                return $string;
        }
    }

    public static function stringContains($hs, $n)
    {
        return (strpos(strtolower($hs), strtolower($n)) !== false);
    }

    public static function removeEmpties($array)
    {
        foreach ($array as $key => $value) {
            if ($value == "") {
                unset($array[$key]);
            }
        }
        return $array;
    }

    public static function findInArrayValues($array, $find)
    {
        foreach ($array as $v) {
            if (preg_match("/\b$find\b/i", $v)) {
                return true;
            }
        }
    }

}



