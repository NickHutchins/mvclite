<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 23/07/2015
 * Time: 15:23
 */

class TimeFunctions {
    public static function convertMinutesToHours($mins){
        $hours = $mins/60;
        $wholeHours = floor($hours);
        $portion = (($hours - $wholeHours)*60);

        if($portion < 10){
            $portion = "0".$portion;
        }

    return number_format($wholeHours,0).":".$portion;

    }

    public static function convertSecondsToHours($seconds){
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours*3600)) / 60);
        $secs = floor($seconds % 60);


        if($hours < 10){
            $hours = "0".$hours;
        }

        if($mins < 10){
            $mins = "0".$mins;
        }

        if($secs < 10){
            $secs = "0".$secs;
        }


        return $hours.":".$mins.":".$secs;

    }
}