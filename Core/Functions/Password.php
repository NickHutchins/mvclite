<?php

class Password {

    public static function generatePassword($plain, $salt = null){

        if($salt){
            $randSalt = $salt;
        }else{
            $randSalt = md5(date("Y-m-d H:i:s").rand(1, 1000));
        }

        $nonEncrypt = $randSalt.$plain.$randSalt;
        $encrypt = hash("sha384", $nonEncrypt);

        $saltSplit = str_split($randSalt);
        $encryptSplit = str_split($encrypt, 3);

        foreach($encryptSplit as  $key => $third){
            $finalArray[] = $third;
            $finalArray[] = $saltSplit[$key];
        }

        return $finalPass = implode("", $finalArray);

    }

    static public function checkPassword($S_plain, $I_ID){
        //RECEIVES PLAIN TEXT, GETS HASH FROM SAVED PASSWORD, HASHES PLAIN TEXT COMPARES, RETURNS TRUE/FALSE
        //$user = CRUD::retrieve("sy_users", array("ID"=>$I_ID), true);
        //$pass = $user['password']; //ORIGINAL PASSWORD IN DB

        $saltArray = str_split($pass, 4); //NEED TO GET SALT

        foreach ($saltArray as $saltBlock) {
            $salt[] = substr($saltBlock, -1);   //LAST CHARACTER IN EACH BLOCK IS A CHARACTER IN THE PASSWORD SALT
        }

        $wholePass = Password::generatePassword($S_plain, implode("", $salt));

        if ($wholePass === $pass) {
            return true;
        } else {
            return false;
        }

    }
}