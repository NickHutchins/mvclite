<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Nick
 * Date: 29/01/15
 * Time: 10:42
 * To change this template use File | Settings | File Templates.
 */

class SiteSetting {
    public static function get($settingName){

        if($settingName == "homeDomain" && defined('MAINURL')){
            return MAINURL;
        }

        $wh['name'] = $settingName;
        $result = CRUD::retrieve(TABLEPREFIX.'_globalconfig', $wh, true);
        if(isset($result['settingValue'])){
            return $result['settingValue'];
        }else{
            throw new StockCheckerException('Invalid Site setting', 'Who tried to use this site setting?'. $settingName, "Site Setting");
        }
    }
}