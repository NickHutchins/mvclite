<?php

class ValidationFunctions
{

    public static function validateTextString($string, $minLength = null, $maxLength = null, $regex = null)
    {

        if (ctype_alpha($string)) { //STRING IS ALL TEXT

            $strLen = strlen($string);

            if (isset($minLength) && isset($maxLength)) {
                //MIN AND MAX LENGTH SUPPLIED

                if ($strLen >= $minLength && $strLen <= $maxLength) {
                    return true;
                } else {
                    return false;
                }
            }

            if(isset($minLength) && $maxLength == null){
                //IF ONLY MIN LENGTH SUPPLIED
                if ($strLen >= $minLength) {
                    return true;
                } else {
                    return false;
                }
            }

            if(isset($maxLength) && $minLength == null){
                //IF ONLY MAX LENGTH SUPPLIED
                if ($strLen <= $maxLength) {
                    return true;
                } else {
                    return false;
                }
            }

        } else {
            return false;
        }

    }

    public static function validateNumericString($string, $minLength = null, $maxLength = null, $regex = null)
    {

        if (is_numeric($string)) { //STRING IS ALL TEXT

            $strLen = strlen($string);

            if (isset($minLength) && isset($maxLength)) {
                //MIN AND MAX LENGTH SUPPLIED

                if ($strLen >= $minLength && $strLen <= $maxLength) {
                    return true;
                } else {
                    return false;
                }
            }

            if(isset($minLength) && !$maxLength){
                //IF ONLY MIN LENGTH SUPPLIED

                if ($strLen >= $minLength) {
                    return true;
                } else {
                    return false;
                }
            }

            if(isset($maxLength) && !$minLength){
                //IF ONLY MAX LENGTH SUPPLIED
                if ($strLen <= $maxLength) {
                    return true;
                } else {
                    return false;
                }
            }

        } else {
            return false;
        }


    }

    public static function validateEmailAddress($email)
    {
        {

            $isValid = true;
            $atIndex = strrpos($email, "@");
            if(is_bool($atIndex) && !$atIndex) {
                $isValid = false;
            } else {
                $domain    = substr($email, $atIndex+1);
                $local     = substr($email, 0, $atIndex);
                $localLen  = strlen($local);
                $domainLen = strlen($domain);
                if($localLen<1 || $localLen>64) {
                    // local part length exceeded
                    $isValid = false;
                } else {
                    if($domainLen<1 || $domainLen>255) {
                        // domain part length exceeded
                        $isValid = false;
                    } else {
                        if($local[0] == '.' || $local[$localLen-1] == '.') {
                            // local part starts or ends with '.'
                            $isValid = false;
                        } else {
                            if(preg_match('/\\.\\./', $local)) {
                                // local part has two consecutive dots
                                $isValid = false;
                            } else {
                                if(!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
                                    // character not valid in domain part
                                    $isValid = false;
                                } else {
                                    if(preg_match('/\\.\\./', $domain)) {
                                        // domain part has two consecutive dots
                                        $isValid = false;
                                    } else {
                                        if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", "", $local))) {
                                            // character not valid in local part unless
                                            // local part is quoted
                                            if(!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", "", $local))) {
                                                $isValid = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if($isValid && !checkdnsrr($domain, "MX")) {
                    // domain not found in DNS
                    //IF EMAIL PASSES PREGEX AND DOES NOT HAVE VALID MX RECORD
                    $isValid = false;
                }

            }
        }
        return $isValid;
    }

    public static function validateEmailDomain($emailDomain){

        if(!checkdnsrr($emailDomain, "MX")) {
            // domain not found in DNS
            //IF EMAIL PASSES PREGEX AND DOES NOT HAVE VALID MX RECORD
            return false;
        }else{
            return true;
        }

    }

    public static function validateMobileNumber($mobileNum){
        //TODO: THIS FUNCTION NEED ATTENTION...
        if(is_numeric($mobileNum)){
            return true;
        }
        $pattern = "/^07[0-9]{10}$/";

    }

    public static function emailProviderValidation($email){
            //RETURN TRUE IF EMAIL IS BLACKLISTED...

            $blacklistValues = GlobalConfig::getGlobalSettingValue("validation-emailBlacklist");    //CHECK DOMAIN IS NOT FREE EMAIL PROVIDER
            $blacklistArr = explode(",", $blacklistValues);
            $emailArr = explode("@", $email);


            foreach($blacklistArr as $blacklistedEmail){

                if($emailArr[1] === $blacklistedEmail){
                    //EMAIL IS BLACKLISTED
                    return true;
                }

            }
    }

    public static function validateURL($url){
        return true;
    }

    public static function sanitiseInput($data){
        if(is_string($data)){
            return htmlentities($data,ENT_QUOTES);
        }

        if(is_array($data)){
            foreach($data as $k => $d){
                $data[$k] = ValidationFunctions::sanitiseInput($d);
            }
        }
        return $data;
    }

}