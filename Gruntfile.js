
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            jq: {
                src: ['Assets/js/jQuery/*.js','Assets/js/jQuery/plugins/*/.js'],
                dest: 'Assets/js/jq.min.js'
            },
            written: {
                src: ['Assets/js/custom/*.js'],
                dest: 'Assets/js/written.min.js'
            },
            third: {
                src: ['Assets/js/thirdParty/*.js'],
                dest: 'Assets/js/thirdparty.min.js'
            }
        },
        sass:{
            dist: {
                files: [
                    {
                    expand: true,
                    cwd: 'Assets/css/sass',
                    src: ['*.scss'],
                    dest: 'Assets/css/compiled',
                    ext: '.css'
                    },
                    {
                      "Assets/css/imports.min.css":"Assets/css/sass/imports.scss"
                    },
                    {
                    expand: true,
                    cwd: 'Assets/css/thirdparty',
                    src: ['*.scss'],
                    dest: 'Assets/css/thirdparty',
                    ext: '.css'
                    }
                ]


            }
        },
        cssmin: {
            target: {
                files: {
                    'Assets/css/written.min.css': [
                        'Assets/css/compiled/*.css'
                    ],
                    'Assets/css/libs.min.css': [
                        'Assets/css/thirdparty/*.css'
                    ]
                }
            }
        },
        cachebuster:{
            options: {
                format: 'php'
            },
            "Core/cachebuster.php":[
                "Assets/css/imports.min.css",
                "Assets/css/libs.min.css",
                "Assets/css/written.min.css",
                "Assets/js/written.min.js",
                "Assets/js/jq.min.js",
                "Assets/js/thirdparty.min.js"
            ]
        },
        watch:{
            scripts: {
                files: [
                    'Assets/js/*/*.js',
                    'Assets/css/*/*.scss'
                ],
                tasks: ['Development']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-cachebuster');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['Development']);

    grunt.registerTask('Deployed', ['uglify','sass', 'cssmin', 'cachebuster']);
    grunt.registerTask('Development', ['Deployed', 'watch']);
    grunt.registerTask('Staging', ['Deployed', 'watch']);
    grunt.registerTask('Live', ['Deployed', 'watch']);


};
