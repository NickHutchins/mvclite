CREATE DATABASE `System` /*!40100 COLLATE 'utf16_bin' */;
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


CREATE TABLE IF NOT EXISTS `system_exceptions` (
  `ID` int(9) NOT NULL AUTO_INCREMENT COMMENT 'PK for the Exception',
  `code` text COLLATE utf16_bin NOT NULL COMMENT 'Stores the Error Code.',
  `message` longtext COLLATE utf16_bin NOT NULL COMMENT 'Stores the Data Dump/Stack Trace at the time  happened',
  `component` text COLLATE utf16_bin COMMENT 'What Caused the Error?',
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf16 COLLATE='utf16_bin';


CREATE TABLE `system_globalconfig` (
	`ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
	`name` VARCHAR(50) NOT NULL COMMENT 'Name of the setting.' COLLATE 'utf8_bin',
	`settingValue` VARCHAR(500) NOT NULL COMMENT 'Value of the setting' COLLATE 'utf8_bin',
	PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf16 COLLATE='utf16_bin'
;




-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
