<?php

namespace Controller;


class error
{
    public function view_main()
    {
        $pageData = [
            "title" => "NicksMVC::ERROR",
        ];


        return $pageData;
    }

    public function view_404()
    {
        $pageData = [
            "title" => "NicksMVC::404",
        ];

        return $pageData;
    }

    public function view_401()
    {
        $pageData = [
            "title" => "NicksMVC::401",
        ];

        return $pageData;
    }
}
