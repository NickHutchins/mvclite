<?php

namespace Controller;

use \Model\Company;

class home implements \Inter\Controller
{

    public function view_main()
    {


        $pageData = [
            "example" => "Here is the homepage!",
            "title" => "NickMVC::Generic"
        ];


        return $pageData;
    }

    public function handles_own_nav()
    {
        //Intentionally Blank.
    }
}
