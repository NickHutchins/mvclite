<?php
namespace Inter;

Interface Controller {
  public function view_main();
  public function handles_own_nav();
}
