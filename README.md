# Nicks long developed and as yet unamed MVC
Please check the pre requisites for running this MVC first.

##Pre-Requisites (linux)

###Assumptions
That you have a server or environment in which you can host a website.
It needs to run on the *AMP setup with a minimum PHP version of PHP 7.
It is also assumed you can setup and configure apache.

###Packages
You will need to have the following packages installed - 
* NPM 
* NODE
* NODEJS Legacy
* Composer

Please install these using your preferred method.
On Deb Systems -

```sudo apt-get install nodejs-legacy```

```sudo apt-get install node```

```sudo apt-get install npm```

```sudo apt-get install composer```

## Getting the MVC
As yet there is no way to publically obtain this MVC.
Please don't take it!

##Setup

###Create a database
* Please create a database for the system to use.
* Please create a user that can access this database and give it full permissions.

At present the system cannot create its own databases and add its user to it.

At present there is no way to install without a database, as the system logs exceptions to it.
Though I'm sure at some point there will be.


###A few commands
Once you have the MVC base installed you will need to run a few commands to get up and running.


```sh install.sh```
-This does an NPM and Composer Install then asks for DB connection credentials and other config (hopefully noted from earlier), 
and creates files for you, as well as creating the database tables, ready for use by the MVC.

Alternatively you can 
* Run ```npm install```
-This installs all the Node Modules the system needs.

* Run ```composer install```
-This installs composer dependencies.
* create the config file from the example file in the root.
* create your tables using the MySQL file in the root.

##When Developing
Please run the default GRUNT task, as this will run the 
* SASS/SCSS Compiler
* CSS Minification
* JS Minification
* CacheBuster
