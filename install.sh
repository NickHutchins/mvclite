#!/bin/bash -e

if  !(npm install 2>&1 >/dev/null)  ; then
    echo "NPM Install Failed";
    exit 1
fi
echo "NPM Install OK."


if  !(composer install 2>&1 >/dev/null)  ; then
    echo "Composer Install Failed";
    exit 1
fi
echo "Composer Install OK."
echo "----"

echo "This script does not yet create The Database Tables + Example Configs - Please use the example config, and .mysql files in the root."