<?php
    try{
        require 'Core/requires.php';
        $navigation = new CORE_Navigation();
        echo $navigation->get_output();

    }catch(Exception $e){
        if(get_class($e) != EXCEPTIONCLASS){
            $e = new EXCEPTIONCLASS("Non ".EXCEPTIONCLASS, $e->getMessage().$e->getTraceAsString());
            echo $e->getExceptionMessage();
        }else{
            echo $e->getExceptionMessage();
        }
    }
