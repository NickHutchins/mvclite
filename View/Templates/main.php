<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title><?=$title?></title>

    <!-- Page Meta -->
    <meta name="description" content="<?=$description?>">
    <meta name="theme-color" content="<?=$colour?>">
    <link rel="shortcut icon" type="image/x-icon" href="/Assets/img/logo/icon.png" />


    <!-- Define viewport, device friendly. -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <!-- Include the site stylesheets -->
    <link href="/Assets/css/libs.min.css?v=<?=Cachebuster::getHash("Assets/css/libs.min.css")?>" rel="stylesheet" media="all">
    <link href="/Assets/css/written.min.css?v=<?=Cachebuster::getHash("Assets/css/written.min.css")?>" rel="stylesheet" media="all">

    <!-- Include any special stylesheets -->
    <?php foreach($css as $sheet){ ?>
        <link href="/Assets/css/<?=$sheet?>" rel="stylesheet" media="all">
    <?php } ?>

    <!-- Include any external stylesheets -->
    <?php foreach($externalcss as $sheet){ ?>
        <link href="<?=$sheet?>" rel="stylesheet" media="all">
    <?php } ?>

    <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700|Lobster" rel="stylesheet">
</head>

<body>
    <?=View::partialView('Partial/header/header')?>
    <main>
        <?=$existingText?>
    </main>
    <?=View::partialView('Partial/footer/footer')?>

    <script src="/Assets/js/jq.min.js?v=<?=Cachebuster::getHash("Assets/js/jq.min.js")?>"></script>
    <script src="/Assets/js/jq.min.js?v=<?=Cachebuster::getHash("Assets/js/thirdparty.min.js")?>"></script>
    <script src="/Assets/js/written.min.js?v=<?=Cachebuster::getHash("Assets/js/written.min.js")?>"></script>
    <?php foreach($js as $script){ ?>
        <script src="/Assets/js/<?=$script?>"></script>
    <?php } ?>

</body>
</html>
